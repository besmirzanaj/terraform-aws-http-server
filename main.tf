terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "remote" {
    organization = "cla-test"

    workspaces {
      name = "cla-test-work"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = var.terraform_aws_region
  access_key = var.terraform_aws_access_key
  secret_key = var.terraform_aws_secret_key
}

# Create users ssh keypair
resource "aws_key_pair" "bzanaj" {
  key_name   = "bzanaj"
  public_key = var.terraform_bzanaj_public_key
  tags       = {
      Name = "bzanaj"
  }
}

# Creating a custom VPS
resource "aws_vpc" "terraform_vpc_1" {
  cidr_block = var.terraform_vpc_cidr_1
  tags       = {
      Name = "terraform_vpc_1"
  }
}

resource "aws_internet_gateway" "terraform_ig_1" {
  vpc_id = aws_vpc.terraform_vpc_1.id
  tags = {
    Name = "terraform_ig_1"
  }
}

resource "aws_route_table" "terraform_rt_1" {
  vpc_id = aws_vpc.terraform_vpc_1.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.terraform_ig_1.id
  }
  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.terraform_ig_1.id
  }
  tags = {
    Name = "terraform_rt_1"
  }
}

resource "aws_subnet" "terraform_subnet_1" {
  vpc_id     = aws_vpc.terraform_vpc_1.id
  cidr_block = var.terraform_subnet_cidr_1[0].cidr_block
  availability_zone = var.terraform_aws_az_1
  tags = {
    Name = var.terraform_subnet_cidr_1[0].name
  }
}

resource "aws_route_table_association" "terraform_rta_1" {
  subnet_id      = aws_subnet.terraform_subnet_1.id
  route_table_id = aws_route_table.terraform_rt_1.id
}

# allow web traffic
resource "aws_security_group" "terraform_allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.terraform_vpc_1.id

  ingress {
    description      = var.terraform_allowed_ingress_cidrs[0].name
    from_port        = var.terraform_allowed_ingress_cidrs[0].from_port
    to_port          = var.terraform_allowed_ingress_cidrs[0].to_port
    protocol         = var.terraform_allowed_ingress_cidrs[0].protocol
    cidr_blocks      = [var.terraform_allowed_ingress_cidrs[0].cidr_block]
  }
  ingress {
    description      = var.terraform_allowed_ingress_cidrs[1].name
    from_port        = var.terraform_allowed_ingress_cidrs[1].from_port
    to_port          = var.terraform_allowed_ingress_cidrs[1].to_port
    protocol         = var.terraform_allowed_ingress_cidrs[1].protocol
    cidr_blocks      = [var.terraform_allowed_ingress_cidrs[1].cidr_block]
  }
  ingress {
    description      = var.terraform_allowed_ingress_cidrs[2].name
    from_port        = var.terraform_allowed_ingress_cidrs[2].from_port
    to_port          = var.terraform_allowed_ingress_cidrs[2].to_port
    protocol         = var.terraform_allowed_ingress_cidrs[2].protocol
    cidr_blocks      = [var.terraform_allowed_ingress_cidrs[2].cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow web,ssh from singe IP"
  }
}

# private IP
resource "aws_network_interface" "terraform_if_1" {
  subnet_id       = aws_subnet.terraform_subnet_1.id
  private_ips     = ["10.120.1.50"]
  security_groups = [aws_security_group.terraform_allow_tls.id]
}

# elastic public IP
resource "aws_eip" "terraform_publicip_1" {
  vpc                       = true
  network_interface         = aws_network_interface.terraform_if_1.id
  associate_with_private_ip = "10.120.1.50"
  depends_on = [
    aws_internet_gateway.terraform_ig_1
  ]
}

output "terraform_public_server_1" {
  value = aws_eip.terraform_publicip_1.public_ip
}

resource "aws_instance" "terraform_server_1" {
  ami           = "ami-09e67e426f25ce0d7"
  instance_type = "t2.micro"
  key_name      = "bzanaj"
  availability_zone = var.terraform_aws_az_1
  network_interface {
    device_index = 0
    network_interface_id = aws_network_interface.terraform_if_1.id
  }
  user_data = "${file("./scripts/install-apache.sh")}"
  tags = {
    Name = "terraform_ubuntu_webserver_1"
  }
}

output "terraform_private_server_1" {
    value = aws_instance.terraform_server_1.private_ip
  }

output "terraform_serverid_server_1" {
    value = aws_instance.terraform_server_1.id
  }
