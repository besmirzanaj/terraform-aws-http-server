variable "terraform_aws_access_key" {
  description = "aws access key"
  type = string
}

variable "terraform_aws_secret_key" {
  description = "aws secret key"
}

variable "terraform_bzanaj_public_key" {
  description = "ssh key"
}

variable "terraform_vpc_cidr_1" {
  description = "VPC cidr block for terraform"
}

variable "terraform_subnet_cidr_1" {
  description = "subnet cidr block for terraform"
}
variable "terraform_aws_region" {
  description = "aws region to be used"
}

variable "terraform_allowed_ingress_cidrs" {
  description = "allowed inbound cidrs"
}


variable "terraform_aws_az_1" {
  description = "define the availability zone"
}