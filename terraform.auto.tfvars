#terraform_aws_access_key = "YOUR_AWS_ACCESS_KEY"
#terraform_aws_secret_key = "YOUR_AWS_SECRET_KEY"
#terraform_bzanaj_public_key = "YOUR_SSH_KEY"
terraform_vpc_cidr_1 = "10.120.0.0/16"
terraform_subnet_cidr_1 = [{cidr_block = "10.120.1.0/24", name = "private_subnet_1"}, {cidr_block = "10.120.2.0/24", name = "private_subnet_2"} ]
terraform_allowed_ingress_cidrs = [
    {cidr_block = "142.116.63.221/32", name = "https", from_port = "443", to_port = "443", protocol = "tcp"}, 
    {cidr_block = "142.116.63.221/32", name = "http", from_port = "80", to_port = "80", protocol = "tcp"}, 
    {cidr_block = "142.116.63.221/32", name = "http", from_port = "22", to_port = "22", protocol = "tcp"}
    ]
terraform_aws_region = "us-east-1"
terraform_aws_az_1 = "us-east-1c"
